package swingSet2

import swingSet2.components.JTablePanel
import swingSet2.components.TopToolBar

object SwingSet2 {
    val topToolBar = TopToolBar()
    val jTablePanel = JTablePanel()
}